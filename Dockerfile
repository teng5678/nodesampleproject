FROM node:latest

#hen creating subdirectories hanging off from
#a non-existing parent directory(s) you must pass the -p flag to mkdir ...
 #Please update your Dockerfile with
RUN mkdir  -p /app/src

WORKDIR /app/src

# copy files to /app/src but don't have to as we set WORKDIR
COPY ["package.json", "package-lock.json", "/src/app.js","."]

RUN npm install

#copy entire source as we are already in the source
COPY .idea .
#export in order to view from browser
EXPOSE 3000

CMD ["node", "app.js"]